import pygst;
import gobject;

pygst.require("0.10");
gobject.threads_init();

#import signal.signal;
#import sys;

#def interrupt(signal, frame):
#	global pipeline;
#	pipeline.set_state(gst.NULL);

#signal.signal(signal.SIGINT, interrupt);

import gst;

def asr_partial_result(asr, text, uttid):
	struct = gst.Structure("partial_result");
	struct.set_value("hyp", text);
	struct.set_value("uttid", uttid);
	print "asr partial: " + text;
	asr.post_message(gst.message_new_application(asr, struct));

def asr_result(asr, text, uttid):
	struct = gst.Structure("result");
	struct.set_value("hyp", text);
	struct.set_value("uttid", uttid);
	print "asr result: " + text;
	asr.post_message(gst.message_new_application(asr, struct));

def partial_result(hyp, uttid):
	print "partial: " + hyp;

def result(hyp, uttid):
	print "final: " + hyp;

def app_msg(bus, msg):
	print "app_msg";
	msgType = msg.structure.get_name();
	if msgType == 'partial_result':
		print "is partial.";
		partial_result(msg.structure['hyp'], msg.structure['uttid']);
	elif msgType == 'result':
		print "is result.";
		result(msg.structure['hyp'], msg.structure['uttid']);
		global pipeline;
		pipeline.set_state(gst.STATE_PAUSED);


pipeline = gst.parse_launch(' ! '.join(['autoaudiosrc',
                                           'queue',
                                           'audioconvert',
                                           'audioresample',
                                           'queue',
                                           'vader name=vader auto-threshold=true',
                                           'pocketsphinx name=asr',
                                           'fakesink dump=1']))

asr = pipeline.get_by_name("asr");
asr.connect("partial_result", asr_partial_result);
asr.connect("result", asr_result);
asr.set_property("fsg", "./houseCommands.fsg");
asr.set_property("configured", True);

bus = pipeline.get_bus();
bus.add_signal_watch();
bus.connect("message::application", app_msg);

pipeline.set_state(gst.STATE_PLAYING);

gobject.MainLoop().run();
