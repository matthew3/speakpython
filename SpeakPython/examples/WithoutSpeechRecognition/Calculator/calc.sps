((start with)|(set (the)? (answer|number) to)) $a;
@results
	a {'setNum('$a')'}
@

[add] (to (that|it))? $a ((to|plus) (that|it|$b))?;
@results
	0,a {'plus('$a')'}
	0,a,b {'plus('$a', '$b')'}
@

(get)? $a plus $b;
@results
	b {'plus('$b')'}
	a,b {'plus('$a', '$b')'}
@

(minus|subtract) ((it)? by)? $a (from (that|it|$b))?;
@results
	a {'minus('$a');'}
	a,b {'minus('$b', '$a');'}
@

(divided|divide) ((that|it)? by)? ($a) (by (that|it|$b))?;
@results
	a {'divide('$a');'}
	b {'divide('$b');'}
	a,b {'divide('$a', '$b');'}
@

$a divided by $b;
@results
	a,b {'divide('$a','$b')'}
@

[half (it)|halved];
@results
	0 {'divide(2)'}
@

[squared|square it]|[cubed|cube it];
@results
	0 {'pow(2)'}
	1 {'pow(3)'}
@

$a times $b;
@results
	a {'multiply('$a', '$b')'}
@

(times|multiply) ((that|it)? by)? $a (by (that|it|$b))?;
@results
	a {'multiply('$a');'}
	a,b {'multiply('$a', '$b');'}
@

([double]|[triple]|[quadroupal]) (it)?;
@results
	0 {'multiply(2)'}
	1 {'multiply(3)'}
	2 {'multiply(4)'}
@

to the (power of)? $a;
@results
	a {'pow('$a')'}
@

$a ([squared]|[cubed]|(to the (power of)? $b));
@results
	0 {'pow(2)'}
	1 {'pow(3)'}
	a,0 {'pow('$a',2)'}
	a,1 {'pow('$a',3)'}
	a,b {'pow('$a','$b')'}
@

[clear];
@results
	0 {'clear();'}
@

$a;
@results
	a {'setNum('$a')'}
@
