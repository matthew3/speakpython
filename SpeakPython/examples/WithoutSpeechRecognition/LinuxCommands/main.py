import sys;
import os;

from SpeakPython.SpeakPython import SpeakPython;
from SpeakPython.Result import Result;

if len(sys.argv) <= 1:
	inStr = raw_input("Enter your command: ");
else:
	inStr = sys.argv[1];

sp = SpeakPython("linux.db")
r = sp.matchResult(inStr);
if r != None:
	rStr = r.getResult();
	print rStr;
	iret = os.system(rStr);
	if iret != 0:
		print "Process returned with ", iret;
else:
	print "Regexes did not match. Are you sure you covered this case?";
