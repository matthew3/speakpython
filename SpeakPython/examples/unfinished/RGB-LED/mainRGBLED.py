from SpeakPython.SpeakPythonRecognizer import SpeakPythonRecognizer

import time;
import pigpio;

pi = pigpio.pi(); #connect to local pi

#gpio.setmode(gpio.BCM);
#gpio.setwarnings(False);

red = 16;
green = 20;
blue = 21;

def changeColour(r, g, b):
	global pi, red, green, blue;

	pi.set_PWM_dutycycle(red, r);
	pi.set_PWM_dutycycle(green, g);
	pi.set_PWM_dutycycle(blue, b);

def toHexColours(in_str):
	r = int(in_str[0:1], 16);
	g = int(in_str[2:3], 16);
	b = int(in_str[4:5], 16);

	return (r,g,b);

def execute(in_str):
	(r,g,b) = toHexColours(in_str);
	changeColour(r, g, b);

#main
try:
	recog = SpeakPythonRecognizer(execute, "spsColours")
	while True:
		recog.recognize();

except KeyboardInterrupt:
	print "Qutting.";

finally:
	#turn off PWM (try at least)	
	pi.set_PWM_dutycycle(red, 0);
	pi.set_PWM_dutycycle(green, 0);
	pi.set_PWM_dutycycle(blue, 0);

	pi.stop();
