#store all colours with their hex as lowercase equiv.
f = open("colours.txt", "r");

colours = {};

colourStr = '';

for hexStr in f:
	colourStr = f.next();
	colours[hexStr[:-1]] = colourStr.lower()[:-1];
	
f.close();

#output all colours as an SPS rule
f = open("spsColours.sps", "w");

firstColour = True;

spsString = "#colours() = ";
results = "@results\n";

counter = 0;
for key in colours.keys():
	colour = colours[key];

	if firstColour:
		spsString += "[" + colour + "]";
		firstColour = False;
	else:
		spsString += " | [" + colour + "]";

	results += "\t" + str(counter) + " { '" + key + "' }\n";

	counter += 1;

results += "@";

f.write(spsString + ";\n" + results);

f.close();
